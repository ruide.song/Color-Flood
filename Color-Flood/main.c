#include<stdio.h>
#include<termios.h>
#include<sys/types.h>
#include<sys/time.h>
#include<stdlib.h>
#include<unistd.h> 
#include<time.h>
#include<string.h>

#include "grill.h"
#include "fichier.h"
#include "boucle.h"
#include "solveur.h"

/**
 * \file main.c
 * \brief fichier de la fonction principale (boucle de jeu)
 * \author SVPlease

 * \date 24 avril 2017
 */
 
  
int n=8,maxd;
int a[8][8];  
int vis[8][8]; 

/*
@requires None
@assigns None
@return None
*/
/**
 * \brief fonction principale (boucle du jeu)
 */
void dfs2(int r,int c,int col) {     /*更新vis[i][j]数组，给vis[i][j] == 2的变成1，与之相邻的变成2*/  
    
    int dx[] = { 0,1,0,-1 };  
    int dy[] = { 1,0,-1,0 };  
    vis[r][c] = 1;  
    int i;
    for( i=0;i<4;++i) {  
        int x = r+dx[i];  
        int y = c+dy[i];  
        if(x < 0 || x >=n || y < 0 || y >= n ) continue;   
        if(vis[x][y] == 1 ) continue;  
        vis[x][y] = 2;  
        if(a[x][y] == col)  
            dfs2(x,y,col);  
    }  
}  
int H() {  
     int maze[6],cnt = 0;  
     memset(maze,0,sizeof(maze)); 
     int i,j; 
     for( i=0;i<n;i++){  
         for( j=0;j<n;j++){  
             if(vis[i][j]==1) continue;  
             if(!maze[a[i][j]]) { 
		maze[a[i][j]]++;
		  cnt++; }  
         }  
     }  
    return cnt;  
}  
int filled(int col) {  
    int cnt = 0;  
    int i,j;
    for( i=0;i<n;i++)  
        for( j=0;j<n;j++){  
            if(a[i][j] != col) continue;  
            if(vis[i][j] == 2) { /*只有当vis[i][j] == 2且与col同色的才染色 */ 
                cnt++;  
                dfs2(i,j,col); 
            }  
        }  
    return cnt;/*返回染色的个数*/  
}  
  
int dfs(int d, Pile *solution) {  
    if(d == maxd) return H()==0;  
    if( ( H() + d ) > maxd ) return 0;  
      int i;
    int vi[n][n];  
    memcpy(vi,vis,sizeof(vi));  
    for( i=0;i<6;i++) {  
		
            if(filled(i) == 0) continue; /*没有符合要求的i颜色的块与之相邻*/ 
	    solution = empiler(solution,i); 
            if(dfs(d+1,solution)) return 1;  
	    depiler(solution);
            memcpy(vis,vi,sizeof(vis));  
    }  
    return 0;  
}  

int transform(char a){
	/*char color[]={'B','V','R','Y','M','G'};*/
	if(a=='B')	return 0;
	if(a=='V')	return 1;
	if(a=='R')	return 2;
	if(a=='Y')	return 3;
	if(a=='M')	return 4;
	if(a=='G')	return 5;
	return -1;
}

int main() {  
     
     while(1){
	
     	printf("choisir la taille  ");
     	while(~scanf("%d",&n)&&n){
     	srand((unsigned)time(NULL));
		int i,j;
		colorgrill g;
		
		
		g=creat(n);
		init(&g);
	
		/*chercher le meillur solution*/
		for(i=0;i<n;i++){
			for(j=0;j<n;j++)
				a[i][j]=transform(g.grill[i][j]);
		} 
		for(i=0;i<n;i++){
			for(j=0;j<n;j++)
				printf(" %i ",a[i][j]);
			printf("\n");
		} 

        	memset(vis,0,sizeof(vis)); 
	  
        	dfs2(0,0,a[0][0]); 
		Pile solution;
		p_init(&solution);  
       	 	for(maxd = 0; ; ++maxd){ 
			p_clear(&solution);
	            	if(dfs(0,&solution)) break;   
		} 

		/* jeu */
		boucle(maxd, g ,n); 
		printf("une meilleur solution est :");  
		p_disp(solution); 
		char sta;
		sta=statut();
	
		if(sta!='y')
		     return 0;
		   
		printf("choisir la taille  ");

		
	}
        }
    return 0;  
}  

	




