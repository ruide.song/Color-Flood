#include<stdio.h>
#include<termios.h>
#include<sys/types.h>
#include<sys/time.h>
#include<stdlib.h>
#include<unistd.h> 
#include<time.h>
#include<string.h>

#define PILE_FIXED_SIZE 100
struct Pile
{
  int *tab;
  int it; 
};
typedef struct Pile Pile;

 
int n=8,maxd;
int a[8][8];  
int vis[8][8];  

void p_init(Pile *p)
{
    int i;
    p->it=-1;
    for(i=0;i<PILE_FIXED_SIZE;i++)
	p->tab=(int *)calloc(PILE_FIXED_SIZE,sizeof(int));
}

Pile p_new(void)
{
    Pile p;
    p_init(&p);
    return p;
}

int p_isEmpty(Pile *p)
{
    return p->it==-1;
}


Pile * empiler(Pile *p, int n){
    if(p->it == PILE_FIXED_SIZE-1){
	printf("pile est plein\n");
        return p;
    }
    p->it = p->it+1;
    p->tab[p->it] = n;
    return p;
}


int depiler(Pile *p){
    if(p_isEmpty(p)){
        return 0;}
    p->it = p->it-1;
    return 1;
}

void p_clear(Pile *p){
    p->it =-1;
}

void p_disp(Pile p){
    char color[]={'B','V','R','Y','M','G'};
    int i;
     printf("[");
    for(i=0; i<=p.it; i++)
    {
        printf(" %c ", color[p.tab[i]]);
    }
    printf("]\n");
    
}

void p_disp1(Pile p){
    
    int i;
     printf("[");
    for(i=0; i<=p.it; i++)
    {
        printf(" %d ",p.tab[i]);
    }
    printf("]\n");
    
}

void copy_pile(Pile *a,Pile *b){
    b->it = a->it;
    int i=0;
    for (i=0;i<=a->it;i++){
        b->tab[i]=a->tab[i];
    }
        
    }



int last(Pile p){
    if (p.it == -1){
        return -1 ;
    }
	
    return p.tab[p.it];
}
void dfs2(int r,int c,int col) {     /*更新vis[i][j]数组，给vis[i][j] == 2的变成1，与之相邻的变成2*/  
    
    int dx[] = { 0,1,0,-1 };  
    int dy[] = { 1,0,-1,0 };  
    vis[r][c] = 1;  
    for(int i=0;i<4;++i) {  
        int x = r+dx[i];  
        int y = c+dy[i];  
        if(x < 0 || x >=n || y < 0 || y >= n ) continue;   
        if(vis[x][y] == 1 ) continue;  
        vis[x][y] = 2;  
        if(a[x][y] == col)  
            dfs2(x,y,col);  
    }  
}  
int H() {  
     int maze[6],cnt = 0;  
     memset(maze,0,sizeof(maze));  
     for(int i=0;i<n;i++){  
         for(int j=0;j<n;j++){  
             if(vis[i][j]==1) continue;  
             if(!maze[a[i][j]]) { 
		maze[a[i][j]]++;
		  cnt++; }  
         }  
     }  
    return cnt;  
}  
int filled(int col) {  
    int cnt = 0;  
    for(int i=0;i<n;i++)  
        for(int j=0;j<n;j++){  
            if(a[i][j] != col) continue;  
            if(vis[i][j] == 2) { /*只有当vis[i][j] == 2且与col同色的才染色 */ 
                cnt++;  
                dfs2(i,j,col); 
            }  
        }  
    return cnt;/*返回染色的个数*/  
}  
  
int dfs(int d, Pile *solution) {  
    if(d == maxd) return H()==0;  
    if( ( H() + d ) > maxd ) return 0;  
      
    int vi[n][n];  
    memcpy(vi,vis,sizeof(vi));  
    for(int i=0;i<6;i++) {  
		
            if(filled(i) == 0) continue; /*没有符合要求的i颜色的块与之相邻*/ 
	    solution = empiler(solution,i); 
            if(dfs(d+1,solution)) return 1;  
	    depiler(solution);
            memcpy(vis,vi,sizeof(vis));  
    }  
    return 0;  
}  
int main() {  
     srand((unsigned)time(NULL));
	int i,j;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++)
			a[i][j]=(rand()%6);
	} 
	for(i=0;i<n;i++){
		for(j=0;j<n;j++)
			printf(" %i ",a[i][j]);
		printf("\n");
	} 
        memset(vis,0,sizeof(vis)); 
	  
        dfs2(0,0,a[0][0]); 
	Pile solution;
	p_init(&solution);  
        for(maxd = 0; ; ++maxd){ 
	p_clear(&solution);
	
            if(dfs(0,&solution)) break;   
	}
        printf("%d\n",maxd); 
	p_disp1(solution);    
      
    return 0;  
}  
