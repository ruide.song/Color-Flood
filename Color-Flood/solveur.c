#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include<termios.h>
#include<sys/types.h>
#include<sys/time.h>
#include<unistd.h> 
#include "grill.h"
#include "solveur.h"

#define PILE_FIXED_SIZE 100

/**
 * \brief initialise une pile d'éléments en lui allouant la mémoire nécessaire
 * \param p un pointeur vers une pile
 * \return p la pile initialisée
 */
void p_init(Pile *p)
{
    int i;
    p->it=-1;
    for(i=0;i<PILE_FIXED_SIZE;i++)
	p->tab=(int *)calloc(PILE_FIXED_SIZE,sizeof(int));
}

/**
 * \brief crée une nouvelle pile non initialisée (vide)
 * \return p la pile initialisée
 */
Pile p_new(void)
{
    Pile p;
    p_init(&p);
    return p;
}

/**
 * \brief vérifie si une pile est vide
 * \param p un pointeur vers la pile dont on veut vérifier la vacuité
 * \return le booléen correspondant
 */
int p_isEmpty(Pile *p)
{
    return p->it==-1;
}

/**
 * \brief empile l'élément n sur la pile entrée en paramètre
 * \param p un pointeur vers une pile
 * \param n un entier
 * \return la pile avec n empilé
 */
Pile * empiler(Pile *p, int n){
    if(p->it == PILE_FIXED_SIZE-1){
	printf("pile est plein\n");
        return p;
    }
    p->it = p->it+1;
    p->tab[p->it] = n;
    return p;
}

/**
 * \brief dépile le dernier élément de la pile entrée en paramètre
 * \param p un pointeur vers une pile
 * \return le dernier élément, dépilé, de la pile
 */
int depiler(Pile *p){
    if(p_isEmpty(p)){
        return 0;}
    p->it = p->it-1;
    return 1;
}


void p_clear(Pile *p){
    p->it =-1;
}



/**
 * \brief affiche une pile
 * \param p une pile à afficher
 */
void p_disp(Pile p){
    char color[]={'B','V','R','Y','M','G'};
    int i;
     printf("[");
    for(i=0; i<=p.it; i++)
    {
        printf(" %c ", color[p.tab[i]]);
    }
    printf("]\n");
    
}


void p_disp1(Pile p){
    
    int i;
     printf("[");
    for(i=0; i<=p.it; i++)
    {
        printf(" %d ",p.tab[i]);
    }
    printf("]\n");
    
}

/**
 * \brief copie une pile dans une autre
 * \param a un pointeur vers une pile à copier
 * \param b un pointeur vers une pile à écraser
 */
void copy_pile(Pile *a,Pile *b){
    b->it = a->it;
    int i=0;
    for (i=0;i<=a->it;i++){
        b->tab[i]=a->tab[i];
    }
        
    }
    
/**
 * \brief copie une grille dans une autre
 * \param g1 un pointeur vers une grille à copier
 * \param g un pointeur vers une grille à écraser
 */
void copy_grill(colorgrill *g,colorgrill  *g1){
    int i,j;
    for(i=0;i<g->size;i++)
        for(j=0;j<g->size;j++){
            g->grill[i][j] = g1->grill[i][j];
        }
    g->now_color=g1->now_color;
    
}

/**
 * \brief prend la dernière solution et la solution temporaire, et copie la meilleure dans la moins bonne
 * \param p un pointeur vers la dernière solution trouvée
 * \param meilleur un pointeur vers la meilleure solution trouvée temporaire
 */
void uneSolutionTrouvee(Pile *p,Pile *meilleur){
	if(meilleur->it==-1){
        copy_pile(p, meilleur);
    }    

    if(meilleur->it>p->it){
        copy_pile(p, meilleur);
    }
}


/*int meme(colorgrill g1,colorgrill g2){
    int i,j;
    for(i=0;i<g1.size;i++){
        for(j=0;j<g1.size;j++){
            if(g1.grill[i][j] != g2.grill[i][j]){
                return 0;
            }
            }
        }
    return 1;

}*/

/**
 * \brief retourne le dernier élément d'une pile
 * \param p une pile
 * \return it le dernier élément de p
 */
int last(Pile p){
    if (p.it == -1){
        return -1 ;
    }
	
    return p.tab[p.it];
}



/**
 * \brief solveur du problème colorflood
 * \param g une grille de couleurs
 * \param n la longueur de la grille
 * \param solution un pointeur vers la pile solution
 * \param profondeur la profondeur de la fonction, qui est récursive
 * \param meilleur un pointeur vers la meilleure solution temporaire
 */
void solveur(colorgrill g, int n, Pile *solution, int profondeur,Pile *meilleur) {
    
    int i;
    colorgrill g2;
    g2 =creat(g.size);
    init(&g2);
    char color[]={'B','V','R','Y','M','G'};
    if(meilleur->it>-1)
	n=meilleur->it;
    for (i=0; i<6; i=i+1) {     /*  toutes les couleurs */
        
        copy_grill(&g2, &g);
	if (last(*solution)==i)
	{	
		continue;
	}
        solution = empiler(solution,i);
	/*p_disp(*solution);*/
        change(&g2, color[i],1,1);
        g2.now_color=color[i];
        /*print_grill(&g2);*/
        if (status(&g2)){
            uneSolutionTrouvee(solution,meilleur);
            depiler(solution);
            continue;
        }
        if(n>profondeur ){
            solveur(g2, n, solution, profondeur+1,meilleur);
         
        }
        else {
            depiler(solution);
            
            
        }
    }
        
        depiler(solution);
}


