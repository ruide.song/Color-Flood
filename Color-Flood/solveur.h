#ifndef _SOLVEUR_H
#define _SOLVEUR_H


/**
 * \struct Pile
 * \brief structure d'une pile d'entier
 */

struct Pile
{
  int *tab; /*!< tableau de stockage de donnée */
  int it;  /*!< position du premier élément de la pile */
};
typedef struct Pile Pile;


void copy_grill(colorgrill *g,colorgrill *g1);
void p_init(Pile *p);
Pile p_new(void);
int p_isEmpty(Pile *p);
Pile *empiler(Pile *p, int n);
int depiler(Pile *p);
void p_clear(Pile *p);
void p_disp(Pile p);
void p_disp1(Pile p);
void copy_pile(Pile *a,Pile *b);
void uneSolutionTrouvee(Pile *p,Pile *meilleur);
int meme(colorgrill g1,colorgrill g2);
int lastele(Pile p);
void solveur(colorgrill g, int n, Pile *solution, int profondeur,Pile *meilleur) ;

#endif
